const unitLength = 20;
let boxColor = '#969696';
const strokeColor = 50;
let columns; /* To be determined by window width */
let rows;    /* To be determined by window height */
let currentBoard;
let nextBoard;
let isPlayingGame = false;
let fps = 30; // Set default fps = 30

function setup() {
    /* Set the canvas to be under the element #canvas*/
    const canvas = createCanvas(windowWidth * 0.9, (windowHeight - 100) * 0.9);
    canvas.parent(document.querySelector('#canvas'));

    /*Calculate the number of columns and rows */
    columns = floor(width / unitLength);
    rows = floor(height / unitLength);

    /*Making both currentBoard and nextBoard 2-dimensional matrix that has (columns * rows) boxes. */
    currentBoard = [];
    nextBoard = [];
    for (let i = 0; i < columns; i++) {
        currentBoard[i] = [];
        nextBoard[i] = []
    }
    // Now both currentBoard and nextBoard are array of array of undefined values.
    init();  // Set the initial values of the currentBoard and nextBoard
}

/**
* Initialize/reset the board state
*/
function init() {
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            currentBoard[i][j] = 0;
            nextBoard[i][j] = 0;
        }
    }
}

function draw() {
    background(255);
    generate();
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            if (currentBoard[i][j] == 1) {
                fill(boxColor);
            } else {
                fill(255);
            }
            stroke(strokeColor);
            rect(i * unitLength, j * unitLength, unitLength, unitLength);
        }
    }
}

function generate() {
    //Loop over every single box on the board
    for (let x = 0; x < columns; x++) {
        for (let y = 0; y < rows; y++) {
            // Count all living members in the Moore neighborhood(8 boxes surrounding)
            let neighbors = 0;
            for (let i of [-1, 0, 1]) {
                for (let j of [-1, 0, 1]) {
                    if (i == 0 && j == 0) {
                        // the cell itself is not its own neighbor
                        continue;
                    }
                    // The modulo operator is crucial for wrapping on the edge
                    neighbors += currentBoard[(x + i + columns) % columns][(y + j + rows) % rows];
                }
            }

            // Rules of Life
            if (currentBoard[x][y] == 1 && neighbors < 2) {
                // Die of Loneliness
                nextBoard[x][y] = 0;
            } else if (currentBoard[x][y] == 1 && neighbors > 3) {
                // Die of Overpopulation
                nextBoard[x][y] = 0;
            } else if (currentBoard[x][y] == 0 && neighbors == 3) {
                // New life due to Reproduction
                nextBoard[x][y] = 1;
            } else {
                // Stasis
                nextBoard[x][y] = currentBoard[x][y];
            }
        }
    }

    // Swap the nextBoard to be the current Board
    [currentBoard, nextBoard] = [nextBoard, currentBoard];
}

/**
 * When mouse is dragged
 */
function mouseDragged() {
    /**
     * If the mouse coordinate is outside the board
     */
    if (mouseX > unitLength * columns || mouseY > unitLength * rows) {
        return;
    }
    const x = Math.floor(mouseX / unitLength);
    const y = Math.floor(mouseY / unitLength);
    currentBoard[x][y] = 1;
    fill(boxColor);
    stroke(strokeColor);
    rect(x * unitLength, y * unitLength, unitLength, unitLength);
}

/**
 * When mouse is pressed
 */
function mousePressed() {
    noLoop();
    if (isPlayingGame) {
        mouseDragged();
    }
}

/**
 * When mouse is released
 */
function mouseReleased() {
    if (isPlayingGame) {
        loop();
    }
}

function windowResized() {
    resizeCanvas(windowWidth * 0.9, (windowHeight - 100) * 0.9);
}

function randomLifeCell() {
    // Random live cell to board
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            const box = Math.round(Math.random());
            currentBoard[i][j] = box;
            nextBoard[i][j] = box;
        }
    }
}

function startGame(start) {
    if (start) {
        // Start the game
        isPlayingGame = true;
        document.querySelector('#start-stop-game').innerText = 'Stop';
        frameRate(fps);
        loop();
    } else {
        // Stop the game
        isPlayingGame = false;
        document.querySelector('#start-stop-game').innerText = 'Start';
        noLoop();
    }
}

function closeSettingModal() {
    const settingModal = bootstrap.Modal.getOrCreateInstance(document.querySelector('#setting-modal'))
    settingModal.hide();
}

document.querySelector('#reset-game')
    .addEventListener('click', function () {
        startGame(false);
        init();
        draw();
    });

document.querySelector('#start-stop-game')
    .addEventListener('click', function () {
        startGame(!isPlayingGame);
    });

document.querySelector('#random-game')
    .addEventListener('click', function () {
        randomLifeCell();
        draw()
    });

document.querySelector('#setting-modal')
    .addEventListener('show.bs.modal', function () {
        // FPS Setting
        document.querySelector('#fps-setting-control').value = fps;
        document.querySelector('#fps-setting-num').innerHTML = fps;

        // Color of life
        document.querySelector('#cell-color-setting-control').value = boxColor;
    })

document.querySelector('#fps-setting-control')
    .addEventListener('input', function () {
        document.querySelector('#fps-setting-num').innerHTML = document.querySelector('#fps-setting-control').value;
    })

document.querySelector('#fps-setting-control')
    .addEventListener('change', function () {
        document.querySelector('#fps-setting-num').innerHTML = document.querySelector('#fps-setting-control').value;
    })


document.querySelector('#save-setting')
    .addEventListener('click', function () {
        // FPS
        fps = parseInt(document.querySelector('#fps-setting-control').value);
        frameRate(fps);

        // Color of life
        boxColor = document.querySelector('#cell-color-setting-control').value;
        draw();

        closeSettingModal();
    });

window.addEventListener("resize", function () {
    resizeCanvas(windowWidth * 0.9, (windowHeight - 100) * 0.9);
});

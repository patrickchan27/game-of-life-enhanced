# Personal Project : Game of Life Enhanced

Here are some of the topics that you need to work on:

- Landing Page for your Game of Life project. It should be a different html file from the Game of life page.
- A simple page to introduce yourself

Here are some of the topics that you should attempt to work on:

- Control speed of the `Game of Life`. (Checkout `framerate`, you can use `slider` to control the framerate )
- Allow us to change the rules of survival.
- Allow users to change the rules of reproduction.
- Start/Stop the Game of life
- Multiple colors of life on the same board.
- Darken colors for stable life.
- Random initial states
- Well-known patterns of Game of Life to select from(Examples:Gosper Glider Gun, Glider, Lightweight train).
- Use Keyboard to control the cursor to place the life
- Resize board on windows resize(Check out `windowsResized`)
- Switching between different styles.
- Anything else that you could think of.